import React from "react"
import PropTypes from "prop-types"
// import styled from "styled-components"
const propTypes = {
}

const defaultProps = {
}

export default class tags extends React.Component {
    state={
    name:'naga'
    }
    changeName=()=>{
        this.setState({
            name:this.state.name==='naga'?"raj":'naga'
        })
    }
    componentWillMount() 
    { 
        console.log("componentWillMount:",this.state.name); 
    } 
  
    componentDidMount() 
    { 
        console.log("componentDidMount()",this.state.name); 
    } 
    componentWillUpdate() 
    { 
        console.log("componentWillUpdate()",this.state.name); 
    } 
  
    componentDidUpdate() 
    { 
        console.log("componentDidUpdate()",this.state.name); 
    } 
  render() {
    return (
      <React.Fragment>
          <h2>state Value: {this.state.name}</h2>
          <button onClick={this.changeName}>changename</button>
    </React.Fragment>
    )
  }
}

tags.propTypes = propTypes
tags.defaultProps = defaultProps
