import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { SketchPicker } from 'react-color';
import { changeColor } from '../actions/postAction';
 class colorPicker extends Component {
     handleChangeComplete=(color)=>{
         console.log('color',color.hex)
         this.props.changeColor(color.hex)
     }
    render() {
        return (
            <div style={{display:"flex",width:"100%",margin:'20px 0px'}}>
            <div style={{margin:"0px 20px"}}>
               <SketchPicker onChangeComplete={ this.handleChangeComplete }/> 
            </div>
            <div style={{width:"200px",height:"200px",background:this.props.bgColor==="#fff"?"#000":this.props.bgColor}}>
            </div>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    bgColor:state.posts.color
  });
export default connect(mapStateToProps,{changeColor})(colorPicker);