import { FETCH_POSTS, NEW_POST, BG_COLOR } from '../actions/types';

const initialState = {
  items: [],
  item: {},
  color:'#fff'
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_POSTS:
      return {
        ...state,
        items: action.payload
      };
    case NEW_POST:
      return {
        ...state,
        item: action.payload
      };
      case BG_COLOR:
          console.log('bgColor',action.payload)
        return {
          ...state,
          color: action.payload
        };
    default:
      return state;
  }
}
