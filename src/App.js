import React, { useState } from 'react';
import logo from './logo.svg';
import Sample from './sample.js'
import {Provider} from 'react-redux'
// import {createStore,applyMiddleware} from 'redux'
import Post from './components/post'
import PostForm from './components/postForm'
import ColorPicker from './components/colorPicker'

import store from './store'
import './App.css';
// const store=createStore(()=>[],{},applyMiddleware())
function App() {
  // const [count, setCount] = useState(0);
  return (
    <Provider store={store}>
    <div className="App">
      <ColorPicker/>
      {/* <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Click me
      </button>
    </div> */}
    {/* <Sample/> */}
    <PostForm/>
    <Post/>
    </div>
    </Provider>
  );
}

export default App;
